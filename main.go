/*
 * @Author: your name
 * @Date: 2021-01-05 13:25:13
 * @LastEditTime: 2021-01-06 20:46:32
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: \lvyou-lib\main.go
 */
package main

import (
	"clib/src/clib"
	"encoding/json"
	"fmt"
)

func main() {
	product()
	// p := make([]clib.Person, 0)

	// p = append(p, clib.Person{
	// 	Name:            "陈鹏",
	// 	Mobile:          "18275130761",
	// 	Credentials:     "520121199312150017",
	// 	CredentialsType: "ID_CARD",
	// })
	// orderParams := clib.OrderParams{
	// 	OrderID:         "123125252125215",
	// 	OrderQuantity:   "1",
	// 	OrderPrice:      "1",
	// 	OrderStatus:     "PREPAY_ORDER_NOT_PAYED",
	// 	ResourceID:      "7363",
	// 	TimeOfBookID:    "0",
	// 	ProductName:     "测试票(非实名制）",
	// 	VisitDate:       "2021-01-10",
	// 	SellPrice:       "1",
	// 	Name:            p[0].Name,
	// 	Mobile:          p[0].Mobile,
	// 	Credentials:     p[0].Credentials,
	// 	CredentialsType: p[0].CredentialsType,
	// 	Persons:         p,
	// }
	// checkOrder(orderParams)
	// createOrder(orderParams)
	payOrder()

}

func product() {
	result, err := clib.GetProductByOTA(clib.ProductByOTAParams{
		Method:      "ALL",
		PageSize:    "10",
		CurrentPage: "1",
	})
	if err != nil {
		fmt.Println(err.Error())
		return
	}
	str, err := json.Marshal(result)
	if err != nil {
		fmt.Println(err.Error())
		return
	}

	fmt.Println(string(str))
}

func checkOrder(orderParams clib.OrderParams) {

	err := clib.CheckOrder(orderParams)
	if err != nil {
		fmt.Println(err.Error())
		return
	}

}

func createOrder(orderParams clib.OrderParams) {

	res, err := clib.CreateOrder(orderParams)
	if err != nil {
		fmt.Println(err.Error())
		return
	}
	str, err := json.Marshal(res)
	if err != nil {
		fmt.Println(err.Error())
		return
	}
	// 90210106999000027  PREPAY_ORDER_NOT_PAYED
	fmt.Println("createorder: ", string(str))

}

func payOrder() {

	res, err := clib.PayOrder(clib.PayOrderParams{
		PartnerOrderID: "90210106999000030",
		OrderPrice:     "1",
		OrderStatus:    "PREPAY_ORDER_NOT_PAYED",
	})
	if err != nil {
		fmt.Println("pay error ", err.Error())
		return
	}
	str, err := json.Marshal(res)
	if err != nil {
		fmt.Println(err.Error())
		return
	}
	fmt.Println("PayOrder: ", string(str))

}
