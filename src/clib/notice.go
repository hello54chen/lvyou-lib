/*
 * @Author: your name
 * @Date: 2021-01-06 18:35:15
 * @LastEditTime: 2021-01-06 19:29:31
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: \lvyou-lib\src\clib\notice.go
 */
package clib

import "fmt"

//NoticeOrderConsumed 用户消费通知
//TODO:  用户消费通知
func NoticeOrderConsumed(reqParams NoticeOrderConsumedParams) (result string, err error) {
	method := "noticeOrderConsumed"

	d := `<?xml version="1.0" encoding="utf-8"?>
	<request xmlns:qm="http://tour.ectrip.com/2014/QMRequestDataSchema" xsi:schemaLocation="http://tour.ectrip.com/2014/QMResponseSchema QMRequestDataSchema-1.1.0.xsd" 
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<header>
			<application>tour.ectrip.com</application>
			<processor>DataExchangeProcessor</processor>
			<version>v1.0.0</version>
			<bodyType>` + method + `</bodyType>
			<createUser>` + CreateUser + `</createUser>
			<createTime>` + GetTime() + `</createTime>
			<supplierIdentity>` + SupplierIdentity + `</supplierIdentity>
		</header>
		<body xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:type="` + method + `">
			<orderInfo>
				<partnerorderId>` + reqParams.PartnerOrderID + `</partnerorderId>
				<otaorderId>` + reqParams.PartnerOrderID + `</otaorderId>
				<orderId>` + reqParams.PartnerOrderID + `</orderId>
				<orderQuantity>` + reqParams.PartnerOrderID + `</orderQuantity>
				<useQuantity>` + reqParams.PartnerOrderID + `</useQuantity>
				<useTotalQuantity>` + reqParams.PartnerOrderID + `</useTotalQuantity>
				<consumeInfo>{"passengers":"[\"441230348\"]"}</consumeInfo>
				<consumedSeq>` + reqParams.PartnerOrderID + `</consumedSeq>
			</orderInfo>
		</body>
	</request>`

	fmt.Println(method, "params:", d)

	//提交请求
	body, err := RequestServer(method, d)
	if err != nil {
		return
	}

	message := body.SelectElement("message")

	result = message.Text()
	return
}

//NoticeOrderRefundApproveResult  申请退款结果通知
//TODO:  申请退款结果通知
func NoticeOrderRefundApproveResult(reqParams NoticeOrderConsumedParams) (result string, err error) {
	method := "noticeOrderRefundApproveResult"

	d := `<?xml version="1.0" encoding="utf-8"?>
	<request xmlns:qm="http://tour.ectrip.com/2014/QMRequestDataSchema" xsi:schemaLocation="http://tour.ectrip.com/2014/QMResponseSchema QMRequestDataSchema-1.1.0.xsd" 
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<header>
			<application>tour.ectrip.com</application>
			<processor>DataExchangeProcessor</processor>
			<version>v1.0.0</version>
			<bodyType>` + method + `</bodyType>
			<createUser>` + CreateUser + `</createUser>
			<createTime>` + GetTime() + `</createTime>
			<supplierIdentity>` + SupplierIdentity + `</supplierIdentity>
		</header>
		<body xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:type="` + method + `">
			<orderInfo>
				<partnerorderId>` + reqParams.PartnerOrderID + `</partnerorderId>
				<otaorderId>` + reqParams.PartnerOrderID + `</otaorderId>
				<orderId>` + reqParams.PartnerOrderID + `</orderId>
				<orderQuantity>` + reqParams.PartnerOrderID + `</orderQuantity>
				<useQuantity>` + reqParams.PartnerOrderID + `</useQuantity>
				<useTotalQuantity>` + reqParams.PartnerOrderID + `</useTotalQuantity>
				<consumeInfo>{"passengers":"[\"441230348\"]"}</consumeInfo>
				<consumedSeq>` + reqParams.PartnerOrderID + `</consumedSeq>
			</orderInfo>
		</body>
	</request>`

	fmt.Println(method, "params:", d)

	//提交请求
	body, err := RequestServer(method, d)
	if err != nil {
		return
	}

	message := body.SelectElement("message")

	result = message.Text()
	return
}

//NoticeOrderPrintSuccess  出票结果通知(预下单模式)
//TODO:  出票结果通知(预下单模式)
func NoticeOrderPrintSuccess(reqParams NoticeOrderPrintSuccessParams) (result string, err error) {
	method := "noticeOrderPrintSuccess"

	d := `<?xml version="1.0" encoding="utf-8"?>
	<request xmlns:qm="http://tour.ectrip.com/2014/QMRequestDataSchema" xsi:schemaLocation="http://tour.ectrip.com/2014/QMResponseSchema QMRequestDataSchema-1.1.0.xsd" 
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<header>
			<application>tour.ectrip.com</application>
			<processor>DataExchangeProcessor</processor>
			<version>v1.0.0</version>
			<bodyType>` + method + `</bodyType>
			<createUser>` + CreateUser + `</createUser>
			<createTime>` + GetTime() + `</createTime>
			<supplierIdentity>` + SupplierIdentity + `</supplierIdentity>
		</header>
		<body xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:type="` + method + `">
			<orderInfo>
				<partnerorderId>` + reqParams.PartnerOrderID + `</partnerorderId>
				<otaorderId>` + reqParams.PartnerOrderID + `</otaorderId>
				<distributorOrderId>` + reqParams.DistributorOrderID + `</distributorOrderId>
				<description>` + reqParams.Description + `</description>
				<orderStatus>` + reqParams.OrderStatus + `</orderStatus>
				<qrCodeStr>` + reqParams.QRCodeStr + `</qrCodeStr>
				<qrCodeUrl>` + reqParams.QRCodeURL + `</qrCodeUrl>
				<verifyCode>` + reqParams.VerifyCode + `</verifyCode>
				<vouchers></vouchers>
			</orderInfo>
		</body>
	</request>`

	fmt.Println(method, "params:", d)

	//提交请求
	body, err := RequestServer(method, d)
	if err != nil {
		return
	}

	message := body.SelectElement("message")

	result = message.Text()
	return
}
