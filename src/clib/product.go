/*
 * @Author: your name
 * @Date: 2021-01-05 13:31:36
 * @LastEditTime: 2021-01-06 19:28:04
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: \lvyou-lib\clib\order.go
 */

package clib

import "fmt"

//GetProductByOTA OTA获取所有产品信息
//TODO: OTA获取所有产品信息
func GetProductByOTA(reqParams ProductByOTAParams) (FInfo TicketInfo, err error) {

	method := "GetProductByOTA"

	d := `<?xml version="1.0" encoding="utf-8"?>
	<request xmlns="http://tour.ectrip.com/2014/QMRequestDataSchema">
		<header>
			<application>tour.ectrip.com</application>
			<processor>DataExchangeProcessor</processor>
			<version>v1.0.0</version>
			<bodyType>` + method + `</bodyType>
			<createUser>` + CreateUser + `</createUser>
			<createTime>` + GetTime() + `</createTime>
			<supplierIdentity>` + SupplierIdentity + `</supplierIdentity>
		</header>
		<body xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:type="` + method + `">
			<method>` + reqParams.Method + `</method>
			<currentPage>` + reqParams.CurrentPage + `</currentPage>
			<pageSize>` + reqParams.PageSize + `</pageSize>
			<resourceId>` + reqParams.ResourceID + `</resourceId>
		</body>
	</request>`

	fmt.Println(method, "params:", d)

	body, err := RequestServer(method, d)
	if err != nil {
		return
	}

	count := body.SelectElement("count")
	FInfo.Count = count.Text()

	productInfoArr := make([]ProductInfo, 0)
	productInfos := body.SelectElement("productInfos")
	for _, productInfo := range productInfos.SelectElements("productInfo") {
		var info ProductInfo

		baseInfo := productInfo.SelectElement("baseInfo")

		resourceID := baseInfo.SelectElement("resourceId")
		info.ResourceID = resourceID.Text()

		productName := baseInfo.SelectElement("productName")
		info.ProductName = productName.Text()

		sightArr := make([]Sight, 0)
		sights := baseInfo.SelectElement("sights")
		for _, sight := range sights.SelectElements("sight") {
			var sightItem Sight
			sightName := sight.SelectElement("sightName")
			city := sight.SelectElement("city")

			sightItem.City = city.Text()
			sightItem.SightName = sightName.Text()
			sightArr = append(sightArr, sightItem)
		}
		info.Sights = sightArr

		otherConfig := productInfo.SelectElement("otherConfig")
		eticketType := otherConfig.SelectElement("eticketType")
		info.ETicketType = eticketType.Text()

		productDescription := productInfo.SelectElement("productDescription")
		refundOption := productDescription.SelectElement("refundOption")
		canRefund := refundOption.SelectElement("canRefund")
		info.CanRefund = canRefund.Text()

		canOverdueRefund := refundOption.SelectElement("canOverdueRefund")
		info.CanOverdueRefund = canOverdueRefund.Text()

		bookConfig := productInfo.SelectElement("bookConfig")
		paymentType := bookConfig.SelectElement("paymentType")
		bookPersonType := bookConfig.SelectElement("bookPersonType")
		info.PaymentType = paymentType.Text()
		info.BookPersonType = bookPersonType.Text()

		priceConfig := productInfo.SelectElement("priceConfig")
		calendarPrices := priceConfig.SelectElement("calendarPrices")
		prices := make([]CalendarPrice, 0)
		for _, calendarPrice := range calendarPrices.SelectElements("calendarPrice") {
			var price CalendarPrice
			useDate := calendarPrice.SelectElement("useDate")
			marketPrice := calendarPrice.SelectElement("marketPrice")
			sellPrice := calendarPrice.SelectElement("sellPrice")
			sellstock := calendarPrice.SelectElement("sellstock")

			price.UseDate = useDate.Text()
			price.MarketPrice = marketPrice.Text()
			price.SellPrice = sellPrice.Text()
			price.Sellstock = sellstock.Text()
			prices = append(prices, price)
		}
		info.CalendarPrices = prices

		productInfoArr = append(productInfoArr, info)
	}
	FInfo.ProductInfos = productInfoArr

	// a, err := json.Marshal(FInfo)
	// if err != nil {
	// 	return
	// }
	// fmt.Println(string(a))
	return
}
