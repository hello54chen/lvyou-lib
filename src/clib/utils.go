/*
 * @Author: your name
 * @Date: 2021-01-05 16:10:55
 * @LastEditTime: 2021-01-06 19:31:11
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: \lvyou-lib\src\clib\utils.go
 */
package clib

import (
	"crypto/md5"
	"encoding/base64"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"io/ioutil"
	"net/http"
	"net/url"
	"strings"
	"time"

	"github.com/beevik/etree"
)

// Base64EncodeString 编码
func Base64EncodeString(str string) string {
	h := md5.New()
	h.Write([]byte(str))
	return base64.StdEncoding.EncodeToString(h.Sum(nil))
}

// Base64DecodeString 解码
func Base64DecodeString(str string) (string, []byte) {
	resBytes, _ := base64.StdEncoding.DecodeString(str)
	return string(resBytes), resBytes
}

// ToMd5 md5转换
func ToMd5(str string) (string, error) {
	w := md5.New()
	_, err := io.WriteString(w, str)          //将str写入到w中
	return fmt.Sprintf("%x", w.Sum(nil)), err //w.Sum(nil)将w的hash转成[]byte格式
}

// GetTime 获取当前时间
func GetTime() string {
	const shortForm = "2006-01-02 15:04:05"
	t := time.Now()
	temp := time.Date(t.Year(), t.Month(), t.Day(), t.Hour(), t.Minute(), t.Second(), t.Nanosecond(), time.Local)
	str := temp.Format(shortForm)
	return str
}

// RequestServer 请求服务器
func RequestServer(method string, xmlStr string) (result *etree.Element, err error) {

	fmt.Println("method:", method)

	data := base64.StdEncoding.EncodeToString([]byte(xmlStr))

	sign, err := ToMd5(SignKey + data)
	if err != nil {
		return
	}
	sign = strings.ToUpper(sign)

	var requestParams RequestParam
	requestParams.Data = data
	requestParams.Signed = sign
	requestParams.SecurityType = "MD5"

	jsonStr, err := json.Marshal(requestParams)

	if err != nil {
		return
	}

	payload := url.Values{}
	payload.Add("method", method)
	payload.Add("requestParam", string(jsonStr))

	//提交请求
	reqest, err := http.NewRequest("POST", BaseURL, strings.NewReader(payload.Encode()))

	if err != nil {
		return
	}
	//增加header选项
	reqest.Header.Add("content-type", "application/x-www-form-urlencoded;charset=utf-8")
	reqest.Header.Add("cache-control", "no-cache")

	client := &http.Client{Timeout: TimeoutTime * time.Second}

	//处理返回结果
	response, err := client.Do(reqest)
	if err != nil {
		return
	}

	defer response.Body.Close()

	res, err := ioutil.ReadAll(response.Body)
	if err != nil {
		return
	}

	var respon RequestParam
	err = json.Unmarshal(res, &respon)
	if err != nil {
		return
	}

	_, by := Base64DecodeString(respon.Data)

	// fmt.Println("result", da)

	doc := etree.NewDocument()

	if err = doc.ReadFromBytes(by); err != nil {
		return
	}
	root := doc.SelectElement("response")

	header := root.SelectElement("header")
	code := header.SelectElement("code")
	describe := header.SelectElement("describe")
	fmt.Println("response -->", code.Text(), "-->", describe.Text())
	if code.Text() != "1000" {
		err = errors.New(describe.Text())
		return
	}
	result = root.SelectElement("body")
	return
}
