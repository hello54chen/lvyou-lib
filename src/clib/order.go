/*
 * @Author: your name
 * @Date: 2021-01-06 13:03:26
 * @LastEditTime: 2021-01-06 19:38:34
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: \lvyou-lib\src\clib\order.go
 */

package clib

import (
	"fmt"

	"github.com/beevik/etree"
)

// CheckOrder 订单检验接口
// TODO: 订单检验接口
func CheckOrder(reqParams OrderParams) (err error) {
	method := "checkOrder"

	d := `<?xml version="1.0" encoding="utf-8"?>
	<request xmlns:qm="http://tour.ectrip.com/2014/QMRequestDataSchema" xsi:schemaLocation="http://tour.ectrip.com/2014/QMResponseSchema QMRequestDataSchema-1.1.0.xsd" 
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<header>
			<application>tour.ectrip.com</application>
			<processor>DataExchangeProcessor</processor>
			<version>v1.0.0</version>
			<bodyType>` + method + `</bodyType>
			<createUser>` + CreateUser + `</createUser>
			<createTime>` + GetTime() + `</createTime>
			<supplierIdentity>` + SupplierIdentity + `</supplierIdentity>
		</header>
		<body xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:type="` + method + `">
			<orderInfo>
				<orderId>` + reqParams.OrderID + `</orderId>
				<orderQuantity>` + reqParams.OrderQuantity + `</orderQuantity>
				<orderPrice>` + reqParams.OrderPrice + `</orderPrice>
				<orderStatus>PREPAY_ORDER_INIT</orderStatus>
				<product>
					<resourceId>` + reqParams.ResourceID + `</resourceId>
					<visitDate>` + reqParams.VisitDate + `</visitDate>
					<productName>` + reqParams.ProductName + `</productName>
					<sellPrice>` + reqParams.SellPrice + `</sellPrice>
					<timeOfBookId></timeOfBookId>
				</product>
				<contactPerson>
					<name>` + reqParams.Name + `</name>
					<mobile>` + reqParams.Mobile + `</mobile>
					<credentials>` + reqParams.Credentials + `</credentials>
					<credentialsType>ID_CARD</credentialsType>
				</contactPerson>
				<visitPerson></visitPerson>
			</orderInfo>
		</body>
	</request>`
	docReq := etree.NewDocument()

	if err = docReq.ReadFromString(d); err != nil {
		fmt.Println("docReq", err.Error())
		return
	}
	root := docReq.SelectElement("request")

	body := root.SelectElement("body")
	orderInfo := body.SelectElement("orderInfo")
	visitPerson := orderInfo.SelectElement("visitPerson")
	for _, v := range reqParams.Persons {
		person := visitPerson.CreateElement("person")

		name := person.CreateElement("name")
		name.SetText(v.Name)

		credentials := person.CreateElement("credentials")
		credentials.SetText(v.Credentials)

		credentialsType := person.CreateElement("credentialsType")
		credentialsType.SetText(v.CredentialsType)
	}
	str, _ := docReq.WriteToString()

	fmt.Println(method, "params:", d)

	//提交请求
	_, err = RequestServer(method, str)

	return

}

//CreateOrder 创建订单
//TODO: 创建订单
func CreateOrder(reqParams OrderParams) (orderResponse CreateOrderResponse, err error) {
	method := "createOrder"

	d := `<?xml version="1.0" encoding="utf-8"?>
	<request xmlns:qm="http://tour.ectrip.com/2014/QMRequestDataSchema" xsi:schemaLocation="http://tour.ectrip.com/2014/QMResponseSchema QMRequestDataSchema-1.1.0.xsd" 
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<header>
			<application>tour.ectrip.com</application>
			<processor>DataExchangeProcessor</processor>
			<version>v1.0.0</version>
			<bodyType>` + method + `</bodyType>
			<createUser>` + CreateUser + `</createUser>
			<createTime>` + GetTime() + `</createTime>
			<supplierIdentity>` + SupplierIdentity + `</supplierIdentity>
		</header>
		<body xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:type="` + method + `">
			<orderInfo>
				<orderId>` + reqParams.OrderID + `</orderId>
				<orderQuantity>` + reqParams.OrderQuantity + `</orderQuantity>
				<orderPrice>` + reqParams.OrderPrice + `</orderPrice>
				<orderStatus>PREPAY_ORDER_INIT</orderStatus>
				<product>
					<resourceId>` + reqParams.ResourceID + `</resourceId>
					<visitDate>` + reqParams.VisitDate + `</visitDate>
					<productName>` + reqParams.ProductName + `</productName>
					<sellPrice>` + reqParams.SellPrice + `</sellPrice>
					<timeOfBookId></timeOfBookId>
				</product>
				<contactPerson>
					<name>` + reqParams.Name + `</name>
					<mobile>` + reqParams.Mobile + `</mobile>
					<credentials>` + reqParams.Credentials + `</credentials>
					<credentialsType>ID_CARD</credentialsType>
				</contactPerson>
				<visitPerson></visitPerson>
			</orderInfo>
		</body>
	</request>`
	docReq := etree.NewDocument()

	if err = docReq.ReadFromString(d); err != nil {
		fmt.Println("docReq", err.Error())
		return
	}
	root := docReq.SelectElement("request")

	reqBody := root.SelectElement("body")
	orderInfo := reqBody.SelectElement("orderInfo")
	visitPerson := orderInfo.SelectElement("visitPerson")
	for _, v := range reqParams.Persons {
		person := visitPerson.CreateElement("person")

		name := person.CreateElement("name")
		name.SetText(v.Name)

		credentials := person.CreateElement("credentials")
		credentials.SetText(v.Credentials)

		credentialsType := person.CreateElement("credentialsType")
		credentialsType.SetText(v.CredentialsType)
	}
	str, _ := docReq.WriteToString()

	fmt.Println(method, "params:", d)

	//提交请求
	body, err := RequestServer(method, str)
	if err != nil {
		return
	}

	orderInfo = body.SelectElement("orderInfo")

	partnerorderID := orderInfo.SelectElement("partnerorderId")
	orderStatus := orderInfo.SelectElement("orderStatus")
	orderResponse = CreateOrderResponse{
		PartnerOrderID: partnerorderID.Text(),
		OrderStatus:    orderStatus.Text(),
	}
	return

}

//PayOrder  支付订单
//TODO: 支付订单
func PayOrder(reqParams PayOrderParams) (orderResponse PayOrderResponse, err error) {
	method := "payOrder"

	d := `<?xml version="1.0" encoding="utf-8"?>
	<request xmlns:qm="http://tour.ectrip.com/2014/QMRequestDataSchema" xsi:schemaLocation="http://tour.ectrip.com/2014/QMResponseSchema QMRequestDataSchema-1.1.0.xsd" 
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<header>
			<application>tour.ectrip.com</application>
			<processor>DataExchangeProcessor</processor>
			<version>v1.0.0</version>
			<bodyType>` + method + `</bodyType>
			<createUser>` + CreateUser + `</createUser>
			<createTime>` + GetTime() + `</createTime>
			<supplierIdentity>` + SupplierIdentity + `</supplierIdentity>
		</header>
		<body xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:type="` + method + `">
			<orderInfo>
				<partnerorderId>` + reqParams.PartnerOrderID + `</partnerorderId>
				<orderPrice>` + reqParams.OrderPrice + `</orderPrice>
				<orderStatus>` + reqParams.OrderStatus + `</orderStatus>
			</orderInfo>
		</body>
	</request>`

	fmt.Println(method, "params:", d)

	//提交请求
	body, err := RequestServer(method, d)
	if err != nil {
		return
	}

	orderInfo := body.SelectElement("orderInfo")
	partnerorderID := orderInfo.SelectElement("partnerorderId")
	orderStatus := orderInfo.SelectElement("orderStatus")
	qrCodeStr := orderInfo.SelectElement("qrCodeStr")
	qrCodeURL := orderInfo.SelectElement("qrCodeUrl")
	verifyCode := orderInfo.SelectElement("verifyCode")
	vouchers := orderInfo.SelectElement("vouchers")

	list := make([]Voucher, 0)
	for _, item := range vouchers.SelectElements("voucher") {
		var voucher Voucher
		voucherCard := item.SelectElement("voucherCard")
		voucherCode := item.SelectElement("voucherCode")
		voucherCodeURL := item.SelectElement("voucherCodeUrl")

		voucher.VoucherCard = voucherCard.Text()
		voucher.VoucherCard = voucherCode.Text()
		voucher.VoucherCard = voucherCodeURL.Text()
		list = append(list, voucher)
	}

	orderResponse = PayOrderResponse{
		PartnerOrderID: partnerorderID.Text(),
		OrderStatus:    orderStatus.Text(),
		QRCodeStr:      qrCodeStr.Text(),
		QRCodeURL:      qrCodeURL.Text(),
		VerifyCode:     verifyCode.Text(),
		Vouchers:       list,
	}
	return
}

//CreatePaymentOrder 创建已支付订单
//TODO: 创建已支付订单
func CreatePaymentOrder(reqParams OrderParams) (orderResponse PayOrderResponse, err error) {
	method := "createPaymentOrder"

	d := `<?xml version="1.0" encoding="utf-8"?>
	<request xmlns:qm="http://tour.ectrip.com/2014/QMRequestDataSchema" xsi:schemaLocation="http://tour.ectrip.com/2014/QMResponseSchema QMRequestDataSchema-1.1.0.xsd" 
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<header>
			<application>tour.ectrip.com</application>
			<processor>DataExchangeProcessor</processor>
			<version>v1.0.0</version>
			<bodyType>` + method + `</bodyType>
			<createUser>` + CreateUser + `</createUser>
			<createTime>` + GetTime() + `</createTime>
			<supplierIdentity>` + SupplierIdentity + `</supplierIdentity>
		</header>
		<body xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:type="` + method + `">
			<orderInfo>
				<orderId>` + reqParams.OrderID + `</orderId>
				<orderQuantity>` + reqParams.OrderQuantity + `</orderQuantity>
				<orderPrice>` + reqParams.OrderPrice + `</orderPrice>
				<orderStatus>PREPAY_ORDER_PRINTING</orderStatus>
				<product>
					<resourceId>` + reqParams.ResourceID + `</resourceId>
					<visitDate>` + reqParams.VisitDate + `</visitDate>
					<productName>` + reqParams.ProductName + `</productName>
					<sellPrice>` + reqParams.SellPrice + `</sellPrice>
					<timeOfBookId></timeOfBookId>
				</product>
				<contactPerson>
					<name>` + reqParams.Name + `</name>
					<mobile>` + reqParams.Mobile + `</mobile>
					<credentials>` + reqParams.Credentials + `</credentials>
					<credentialsType>ID_CARD</credentialsType>
				</contactPerson>
				<visitPerson></visitPerson>
			</orderInfo>
		</body>
	</request>`
	docReq := etree.NewDocument()

	if err = docReq.ReadFromString(d); err != nil {
		fmt.Println("docReq", err.Error())
		return
	}
	root := docReq.SelectElement("request")
	// for _, v := range root.Attr {
	// 	fmt.Println("-->", )
	// }
	reqBody := root.SelectElement("body")
	orderInfo := reqBody.SelectElement("orderInfo")
	visitPerson := orderInfo.SelectElement("visitPerson")
	for _, v := range reqParams.Persons {
		person := visitPerson.CreateElement("person")

		name := person.CreateElement("name")
		name.SetText(v.Name)

		credentials := person.CreateElement("credentials")
		credentials.SetText(v.Credentials)

		credentialsType := person.CreateElement("credentialsType")
		credentialsType.SetText(v.CredentialsType)
	}
	str, _ := docReq.WriteToString()

	fmt.Println(method, "params:", d)

	//提交请求
	body, err := RequestServer(method, str)
	if err != nil {
		return
	}

	orderInfo = body.SelectElement("orderInfo")
	partnerorderID := orderInfo.SelectElement("partnerorderId")
	orderStatus := orderInfo.SelectElement("orderStatus")
	qrCodeStr := orderInfo.SelectElement("qrCodeStr")
	qrCodeURL := orderInfo.SelectElement("qrCodeUrl")
	verifyCode := orderInfo.SelectElement("verifyCode")
	vouchers := orderInfo.SelectElement("vouchers")

	list := make([]Voucher, 0)
	for _, item := range vouchers.SelectElements("voucher") {
		var voucher Voucher
		voucherCard := item.SelectElement("voucherCard")
		voucherCode := item.SelectElement("voucherCode")
		voucherCodeURL := item.SelectElement("voucherCodeUrl")

		voucher.VoucherCard = voucherCard.Text()
		voucher.VoucherCard = voucherCode.Text()
		voucher.VoucherCard = voucherCodeURL.Text()
		list = append(list, voucher)
	}

	orderResponse = PayOrderResponse{
		PartnerOrderID: partnerorderID.Text(),
		OrderStatus:    orderStatus.Text(),
		QRCodeStr:      qrCodeStr.Text(),
		QRCodeURL:      qrCodeURL.Text(),
		VerifyCode:     verifyCode.Text(),
		Vouchers:       list,
	}
	return

}

//PushOrder 同步订单
//TODO: 同步订单
func PushOrder(reqParams PushOrderParams) (orderResponse PayOrderResponse, err error) {
	method := "pushOrder"

	d := `<?xml version="1.0" encoding="utf-8"?>
	<request xmlns:qm="http://tour.ectrip.com/2014/QMRequestDataSchema" xsi:schemaLocation="http://tour.ectrip.com/2014/QMResponseSchema QMRequestDataSchema-1.1.0.xsd" 
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<header>
			<application>tour.ectrip.com</application>
			<processor>DataExchangeProcessor</processor>
			<version>v1.0.0</version>
			<bodyType>` + method + `</bodyType>
			<createUser>` + CreateUser + `</createUser>
			<createTime>` + GetTime() + `</createTime>
			<supplierIdentity>` + SupplierIdentity + `</supplierIdentity>
		</header>
		<body xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:type="` + method + `">
			<orderInfo>
				<partnerOrderId>` + reqParams.PartnerOrderID + `</partnerOrderId>
				<visitDate>` + reqParams.VisitDate + `</visitDate>
				<contactPerson>
					<name>` + reqParams.Name + `</name>
					<mobile>` + reqParams.Mobile + `</mobile>
					<credentials>` + reqParams.Credentials + `</credentials>
					<credentialsType>` + reqParams.CredentialsType + `</credentialsType>
				</contactPerson>
				<visitPerson></visitPerson>
			</orderInfo>
		</body>
	</request>`
	docReq := etree.NewDocument()

	if err = docReq.ReadFromString(d); err != nil {
		fmt.Println("docReq", err.Error())
		return
	}
	root := docReq.SelectElement("request")

	reqBody := root.SelectElement("body")
	orderInfo := reqBody.SelectElement("orderInfo")
	visitPerson := orderInfo.SelectElement("visitPerson")
	for _, v := range reqParams.Persons {
		person := visitPerson.CreateElement("person")

		name := person.CreateElement("name")
		name.SetText(v.Name)

		credentials := person.CreateElement("credentials")
		credentials.SetText(v.Credentials)

		credentialsType := person.CreateElement("credentialsType")
		credentialsType.SetText(v.CredentialsType)
	}
	str, _ := docReq.WriteToString()
	fmt.Println(method, "params:", d)
	//提交请求
	body, err := RequestServer(method, str)
	if err != nil {
		return
	}

	orderInfo = body.SelectElement("orderInfo")
	partnerorderID := orderInfo.SelectElement("partnerorderId")

	orderResponse = PayOrderResponse{
		PartnerOrderID: partnerorderID.Text(),
	}
	return
}

//GetOrderByOTA OTA获取订单信息
//TODO: OTA获取订单信息
func GetOrderByOTA(partnerOrderID string, orderID string) (orderResponse OrderByOTAResponse, err error) {
	method := "getOrderByOTA"

	d := `<?xml version="1.0" encoding="utf-8"?>
	<request xmlns:qm="http://tour.ectrip.com/2014/QMRequestDataSchema" xsi:schemaLocation="http://tour.ectrip.com/2014/QMResponseSchema QMRequestDataSchema-1.1.0.xsd" 
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<header>
			<application>tour.ectrip.com</application>
			<processor>DataExchangeProcessor</processor>
			<version>v1.0.0</version>
			<bodyType>` + method + `</bodyType>
			<createUser>` + CreateUser + `</createUser>
			<createTime>` + GetTime() + `</createTime>
			<supplierIdentity>` + SupplierIdentity + `</supplierIdentity>
		</header>
		<body xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:type="` + method + `">
			<orderInfo>
				<partnerOrderId>` + partnerOrderID + `</partnerOrderId>
				<orderId>` + orderID + `</orderId>
			</orderInfo>
		</body>
	</request>`

	fmt.Println(method, "params:", d)
	//提交请求
	body, err := RequestServer(method, d)
	if err != nil {
		return
	}

	orderInfo := body.SelectElement("orderInfo")
	partnerorderID := orderInfo.SelectElement("partnerorderId")
	orderStatus := orderInfo.SelectElement("orderStatus")
	totalQuantity := orderInfo.SelectElement("totalQuantity")
	orderQuantity := orderInfo.SelectElement("orderQuantity")
	eticketNo := orderInfo.SelectElement("eticketNo")
	eticketSended := orderInfo.SelectElement("eticketSended")
	useQuantity := orderInfo.SelectElement("useQuantity")
	refundQuantity := orderInfo.SelectElement("refundQuantity")
	consumeInfo := orderInfo.SelectElement("consumeInfo")
	otherQRCode := orderInfo.SelectElement("otherQRCode")
	vouchers := orderInfo.SelectElement("vouchers")

	list := make([]Voucher, 0)
	for _, item := range vouchers.SelectElements("voucher") {
		var voucher Voucher
		voucherCard := item.SelectElement("voucherCard")
		voucherCode := item.SelectElement("voucherCode")
		voucherCodeURL := item.SelectElement("voucherCodeUrl")

		voucher.VoucherCard = voucherCard.Text()
		voucher.VoucherCard = voucherCode.Text()
		voucher.VoucherCard = voucherCodeURL.Text()
		list = append(list, voucher)
	}

	orderResponse = OrderByOTAResponse{
		PartnerOrderID: partnerorderID.Text(),
		OrderStatus:    orderStatus.Text(),
		TotalQuantity:  totalQuantity.Text(),
		OrderQuantity:  orderQuantity.Text(),
		EticketNo:      eticketNo.Text(),
		EticketSended:  eticketSended.Text(),
		UseQuantity:    useQuantity.Text(),
		RefundQuantity: refundQuantity.Text(),
		ConsumeInfo:    consumeInfo.Text(),
		OtherQRCode:    otherQRCode.Text(),
		Vouchers:       list,
	}
	return
}

//SendOrderEticket （重）发入园凭证
//TODO: （重）发入园凭证
//partnerOrderId:由畅游通生成的订单 ID ,phoneNumber:重发短信时的手机号
func SendOrderEticket(partnerOrderID string, phoneNumber string) (orderResponse string, err error) {
	method := "sendOrderEticket"

	d := `<?xml version="1.0" encoding="utf-8"?>
	<request xmlns:qm="http://tour.ectrip.com/2014/QMRequestDataSchema" xsi:schemaLocation="http://tour.ectrip.com/2014/QMResponseSchema QMRequestDataSchema-1.1.0.xsd" 
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<header>
			<application>tour.ectrip.com</application>
			<processor>DataExchangeProcessor</processor>
			<version>v1.0.0</version>
			<bodyType>` + method + `</bodyType>
			<createUser>` + CreateUser + `</createUser>
			<createTime>` + GetTime() + `</createTime>
			<supplierIdentity>` + SupplierIdentity + `</supplierIdentity>
		</header>
		<body xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:type="` + method + `">
			<orderInfo>
				<partnerOrderId>` + partnerOrderID + `</partnerOrderId>
				<phoneNumber>` + phoneNumber + `</phoneNumber>
			</orderInfo>
		</body>
	</request>`

	fmt.Println(method, "params:", d)

	//提交请求
	body, err := RequestServer(method, d)
	if err != nil {
		return
	}

	message := body.SelectElement("message")

	orderResponse = message.Text()
	return
}
