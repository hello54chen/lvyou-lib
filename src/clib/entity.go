/*
 * @Author: your name
 * @Date: 2021-01-06 13:06:20
 * @LastEditTime: 2021-01-06 19:25:48
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: \lvyou-lib\src\clib\entity.go
 */
package clib

/*
订单状态
PREPAY_ORDER_INIT 			预付：初始订单
PREPAY_ORDER_BOOK_FAILED 	预付：预订失败
PREPAY_ORDER_NOT_PAYED 		预付：预订成功，待支付
PREPAY_ORDER_CANCEL 		预付：订单已取消
PREPAY_ORDER_PRINTING 		预付：已付款，出票中
PREPAY_ORDER_PRINT_FAILED 	预付：出票失败
PREPAY_ORDER_PRINT_SUCCESS 	预付：出票成功
PREPAY_ORDER_REFUNDED 		预付：已退订
PREPAY_ORDER_CONSUMED 		预付：已消费
CASHPAY_ORDER_INIT 			现付：初始订单
CASHPAY_ORDER_PRINT_FAILED 	现付：出票失败
CASHPAY_ORDER_REFUNDED	 	现付：已退订
CASHPAY_ORDER_CONSUMED 		现付：已消费
*/

//RequestParam 网络请求参数
type RequestParam struct {
	Data         string `json:"data"`
	Signed       string `json:"signed"`
	SecurityType string `json:"securityType"`
}

//ProductByOTAParams OTA获取产品信息参数
type ProductByOTAParams struct {
	Method      string `json:"method"`
	CurrentPage string `json:"currentPage"`
	PageSize    string `json:"pageSize"`
	ResourceID  string `json:"resourceId"`
}

//TicketInfo 票务信息
type TicketInfo struct {
	Count        string        `json:"count"`
	ProductInfos []ProductInfo `json:"productInfos"`
}

//ProductInfo 产品信息
type ProductInfo struct {
	ResourceID       string          `json:"resourceId"`
	ProductName      string          `json:"productName"`
	PaymentType      string          `json:"paymentType"`
	BookPersonType   string          `json:"bookPersonType"`
	CalendarPrices   []CalendarPrice `json:"calendarPrice"`
	ETicketType      string          `json:"eticketType"`
	CanRefund        string          `json:"canRefund"`
	CanOverdueRefund string          `json:"canOverdueRefund"`
	Sights           []Sight         `json:"sight"`
}

//CalendarPrice 未来一个月每天票务价格
type CalendarPrice struct {
	UseDate     string `xml:"useDate"`
	MarketPrice string `xml:"marketPrice"`
	SellPrice   string `xml:"sellPrice"`
	Sellstock   string `xml:"sellstock"`
}

//Sight 景区
type Sight struct {
	SightName string `xml:"sightName"`
	City      string `xml:"city"`
}

//OrderParams 订单参数
type OrderParams struct {
	OrderID         string   `json:"orderId"`         // OTA订单号
	OrderQuantity   string   `json:"orderQuantity"`   //订单票数
	OrderPrice      string   `json:"orderPrice"`      //订单总价 单位 分
	OrderStatus     string   `json:"orderStatus"`     //订单状态PREPAY_ORDER_NOT_PAYED未支付
	ResourceID      string   `json:"resourceId"`      //供应商产品ID
	TimeOfBookID    string   `json:"timeOfBookId"`    //分时ID,当品类型为分时产品时必传
	ProductName     string   `json:"productName"`     //产品名称
	VisitDate       string   `json:"visitDate"`       //yyyy-MM-dd 产品类型为期票时该字段应为空,否则该期票产品订单不生效
	SellPrice       string   `json:"sellPrice"`       //产品售卖单价	单位：分
	Name            string   `json:"name"`            //订单取票人姓名
	Mobile          string   `json:"mobile"`          //订单取票人的电话
	Credentials     string   `json:"credentials"`     //取票人证件号
	CredentialsType string   `json:"credentialsType"` //取票人证件号 取票人证件类型身份证 : ID_CARD,护照 : HUZHAO,台胞证 : TAIBAO港澳通行证:  GANGAO军官证:JUNGUAN其它：OTHER 配合credentials 生效
	Persons         []Person `json:"visitPerson"`     //游玩人
}

//Person 游客信息
type Person struct {
	Name            string `json:"name"`            //游玩人姓名
	Credentials     string `json:"credentials"`     //游玩人证件号
	CredentialsType string `json:"credentialsType"` //取票人证件类型身份证 : ID_CARD,配合  credentials 生效
	Mobile          string `json:"mobile"`          //游玩人手机号
}

//CreateOrderResponse 创建订单返回
type CreateOrderResponse struct {
	PartnerOrderID string `json:"partnerorderId"` //畅游通的订单 ID
	OrderStatus    string `json:"orderStatus"`    // 订单状态 PREPAY_ORDER_NOT_PAYED 未支付
}

//PayOrderParams 支付订单
type PayOrderParams struct {
	PartnerOrderID string `json:"partnerOrderId"` //畅游通订单ID
	OrderStatus    string `json:"orderStatus"`    //订单状态
	OrderPrice     string `json:"orderPrice"`     //订单金额
}

//PayOrderResponse 支付订单返回
type PayOrderResponse struct {
	PartnerOrderID string    `json:"partnerorderId"` //订单id
	OrderStatus    string    `json:"orderStatus"`    //"订单状态：PREPAY_ORDER_PRINT_SUCCESS 未支付"
	QRCodeStr      string    `json:"qrCodeStr"`      //取票内容，二维码展示用于消费订单
	QRCodeURL      string    `json:"qrCodeUrl"`      //整个订单详情页
	VerifyCode     string    `json:"verifyCode"`     //取票密码
	Vouchers       []Voucher `json:"vouchers"`       //票码集合
}

//Voucher 票券
type Voucher struct {
	VoucherCard    string `json:"voucherCard"`    //实名制产品返回游客身份证
	VoucherCode    string `json:"voucherCode"`    //OTA可凭此内容生成二维码
	VoucherCodeURL string `json:"VoucherCodeUrl"` //游客可凭该链接二维码扫码入园
}

//PushOrderParams 同步订单参数
type PushOrderParams struct {
	PartnerOrderID  string   `json:"partnerOrderId"`  // 畅游通订单ID
	VisitDate       string   `json:"visitDate"`       //yyyy-MM-dd 产品类型为期票时该字段应为空,否则该期票产品订单不生效
	Name            string   `json:"name"`            //订单取票人姓名
	Mobile          string   `json:"mobile"`          //订单取票人的电话
	Credentials     string   `json:"credentials"`     //取票人证件号
	CredentialsType string   `json:"credentialsType"` //取票人证件号 取票人证件类型身份证 : ID_CARD,护照 : HUZHAO,台胞证 : TAIBAO港澳通行证:  GANGAO军官证:JUNGUAN其它：OTHER 配合credentials 生效
	Persons         []Person `json:"visitPerson"`     //游玩人
}

//OrderByOTAResponse OTA获取订单信息返回
type OrderByOTAResponse struct {
	PartnerOrderID string    `json:"partnerOrderId"` // 畅游通订单ID
	OrderStatus    string    `json:"orderStatus"`    //订单状态PREPAY_ORDER_INIT 预付：初始订单PREPAY_ORDER_BOOK_FAILED预付：预订失败PREPAY_ORDER_NOT_PAYED预付：预订成功，待支付PREPAY_ORDER_CANCEL 预付：订单已取消PREPAY_ORDER_PRINTING 预付：已付款，出票中PREPAY_ORDER_PRINT_FAILED预付：出票失败PREPAY_ORDER_PRINT_SUCCE SS 预付：出票成功PREPAY_ORDER_REFUNDED预付：已退订PREPAY_ORDER_CONSUMED预付：已消费CASHPAY_ORDER_INIT现付：初始订单CASHPAY_ORDER_PRINT_FAILED现付：出票失败CASHPAY_ORDER_REFUNDED现付：已退订CASHPAY_ORDER_CONSUMED现付：已消费
	TotalQuantity  string    `json:"totalQuantity"`  //订单总数量
	OrderQuantity  string    `json:"orderQuantity"`  //订单剩余票数
	EticketNo      string    `json:"eticketNo"`      //取票参数:订单号[1],密码[2],二维码链接[3],二维码字符串[4]3、4可能为空
	EticketSended  string    `json:"eticketSended"`  //TRUE：电子票已发送FALSE：电子票未发送如果没有电子票，则记录实际通知用户后，设置为 true返回。
	UseQuantity    string    `json:"useQuantity"`    //已消费票数
	RefundQuantity string    `json:"refundQuantity"` //退订数量
	ConsumeInfo    string    `json:"consumeInfo"`    //消费额外信息，相当于消费时的备注。
	OtherQRCode    string    `json:"otherQRCode"`    //目前支持 智游宝PMS产品
	Vouchers       []Voucher `json:"vouchers"`       //票码集合
}

//ApplyOrderRefundByUserParams 退款申请参数
type ApplyOrderRefundByUserParams struct {
	PartnerOrderID    string   `json:"partnerOrderId"`    // 畅游通订单ID
	RefundSeq         string   `json:"refundSeq"`         //退款流水号，用于标记每一笔 退款，由 OTA 定义
	OrderPrice        string   `json:"orderPrice"`        //原始订单金额单位：分
	OrderQuantity     string   `json:"orderQuantity"`     //orderQuantity String 订单剩余票数 订单剩余票数
	RefundQuantity    string   `json:"refundQuantity"`    //退款票数
	OrderRefundPrice  string   `json:"orderRefundPrice"`  //订单退款的金额单位：分
	OrderRefundCharge string   `json:"orderRefundCharge"` //订单退款的手续费总额单位：分
	RefundExplain     string   `json:"refundExplain"`     //退款说明
	Persons           []Person `json:"visitPerson"`       //游玩人
}

//NoticeOrderConsumedParams 用户消费通知参数
type NoticeOrderConsumedParams struct {
	PartnerOrderID   string `json:"partnerOrderId"`   // 畅游通订单ID
	OtaOrderID       string `json:"otaorderId"`       //OTA订单号
	OrderID          string `json:"orderId"`          //OTA订单号(保留旧字段,不建议使用)
	OrderQuantity    string `json:"orderQuantity"`    //订单剩余票数
	UseQuantity      string `json:"useQuantity"`      //本次消费数量
	UseTotalQuantity string `json:"useTotalQuantity"` //总核销数量
	ConsumedSeq      string `json:"consumedSeq"`      //消费通知唯一标识
	ConsumeInfo      string `json:"consumeInfo"`      //电子票消费信息 consumeInfo(如果是订单是实名制订单 内容为json字符串 如果非实名制 则为 “消费信息”)
}

//NoticeOrderRefundApproveResultParams 用户消费通知参数
type NoticeOrderRefundApproveResultParams struct {
	PartnerOrderID    string `json:"partnerOrderId"`    // 畅游通订单ID
	OtaOrderID        string `json:"otaorderId"`        //OTA订单号
	RefundSeq         string `json:"refundSeq"`         //退款流水号，用于标记每一笔退款，由OTA定义
	OrderQuantity     string `json:"orderQuantity"`     //订单剩余票数
	RefundResult      string `json:"refundResult"`      //APPROVE：同意退款 REJECT：拒绝退款
	Description       string `json:"description"`       //退款返回结果描述
	RefundQuantity    string `json:"refundQuantity"`    //退款票数
	OrderRefundPrice  string `json:"orderRefundPrice"`  //退款金额
	OrderRefundCharge string `json:"orderRefundCharge"` //退款手续费
}

//NoticeOrderPrintSuccessParams 出票结果通知(预下单模式)
type NoticeOrderPrintSuccessParams struct {
	PartnerOrderID     string    `json:"partnerOrderId"`    // 畅游通订单ID
	OtaOrderID         string    `json:"otaorderId"`        //OTA订单号
	DistributorOrderID string    `json:"istributorOrderId"` //OTA订单号(保留旧字段,不建议使用)
	Description        string    `json:"description"`       //具体失败原因描述
	OrderStatus        string    `json:"orderStatus"`       //订单状态:出票失败PREPAY_ORDER_PRINT_FAILED出票成功PREPAY_ORDER_PRINT_SUCCESS
	QRCodeStr          string    `json:"qrCodeStr"`         //取票码内容,OTA可根据该码生成取票二维码展示给游客取票(换票)用于消费订单
	QRCodeURL          string    `json:"qrCodeUrl"`         //取票码地址
	VerifyCode         string    `json:"verifyCode"`        //取票密码
	Vouchers           []Voucher `json:"vouchers"`          //票码集合
}
