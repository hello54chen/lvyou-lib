/*
 * @Author: your name
 * @Date: 2021-01-06 18:32:45
 * @LastEditTime: 2021-01-06 19:27:25
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: \lvyou-lib\src\clib\refound.go
 */
package clib

import "fmt"

// ApplyOrderRefundByUser OTA退款申请
//TODO: OTA退款申请
func ApplyOrderRefundByUser(reqParams ApplyOrderRefundByUserParams) (orderResponse string, err error) {
	method := "applyOrderRefundByUser"

	d := `<?xml version="1.0" encoding="utf-8"?>
	<request xmlns:qm="http://tour.ectrip.com/2014/QMRequestDataSchema" xsi:schemaLocation="http://tour.ectrip.com/2014/QMResponseSchema QMRequestDataSchema-1.1.0.xsd" 
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<header>
			<application>tour.ectrip.com</application>
			<processor>DataExchangeProcessor</processor>
			<version>v1.0.0</version>
			<bodyType>` + method + `</bodyType>
			<createUser>` + CreateUser + `</createUser>
			<createTime>` + GetTime() + `</createTime>
			<supplierIdentity>` + SupplierIdentity + `</supplierIdentity>
		</header>
		<body xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:type="` + method + `">
			<orderInfo>
				<qm:partnerorderId>` + reqParams.PartnerOrderID + `</qm:partnerorderId>
				<qm:refundSeq>` + reqParams.RefundSeq + `</qm:refundSeq>
				<qm:orderQuantity>` + reqParams.OrderQuantity + `</qm:orderQuantity>
				<qm:orderPrice>` + reqParams.OrderPrice + `</qm:orderPrice>
				<qm:refundQuantity>` + reqParams.RefundQuantity + `</qm:refundQuantity>
				<qm:orderRefundPrice>` + reqParams.OrderRefundPrice + `</qm:orderRefundPrice>
				<qm:orderRefundCharge>` + reqParams.OrderRefundCharge + `</qm:orderRefundCharge>
				<qm:refundExplain>` + reqParams.RefundExplain + `</qm:refundExplain>
			</orderInfo>
		</body>
	</request>`

	fmt.Println(method, "params:", d)

	fmt.Println("-------------")
	//提交请求
	body, err := RequestServer(method, d)
	if err != nil {
		return
	}

	message := body.SelectElement("message")

	orderResponse = message.Text()
	return
}
